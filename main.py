from typing import List
from fastapi import FastAPI
from pydantic import BaseModel
from pymongo import MongoClient, ASCENDING
from pymongo.results import DeleteResult
from fastapi.middleware.cors import CORSMiddleware
from bson import ObjectId
import websocket
import asyncio
from pymongo.errors import DuplicateKeyError
from fastapi import Query
from fastapi.responses import JSONResponse



app = FastAPI()
origins = [

    "http://localhost:5173",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


client = MongoClient("mongodb://localhost:27017")
db = client["receipts_db"]
db2 = client["confirmation_db"]


refund_collection = db2["refunds"]
sales_collection = db["sales"]
sales_test_col = db2["sales_test"]
confirmed_receipts = db2['confirmed_receipts']
autofirmed_receipts = db2['auto-confirmed']


collection = db["incoming_receipts"]
test_collection = db["test_collection"]
latest_collection = db["new_receipts2"]
collection = db["saved_checked_items"]


class LineTax(BaseModel):
    # Define the LineTax structure
    # Modify the fields as per your requirements
    tax_name: str
    tax_amount: float


class LineDiscount(BaseModel):
    # Define the LineDiscount structure
    # Modify the fields as per your requirements
    discount_name: str
    discount_amount: float


class LineModifier(BaseModel):
    # Define the LineModifier structure
    # Modify the fields as per your requirements
    modifier_name: str
    modifier_amount: float


class LineItem(BaseModel):
    # Define the LineItem structure
    # Modify the fields as per your requirements
    id: str
    item_id: str
    variant_id: str
    item_name: str
    variant_name: str
    sku: str
    quantity: float
    price: float
    gross_total_money: float
    total_money: float
    cost: float
    cost_total: float
    line_note: str
    line_taxes: List[LineTax] = []
    total_discount: float
    line_discounts: List[LineDiscount] = []
    line_modifiers: List[LineModifier] = []


class Receipt(BaseModel):
    # Define the Receipt structure
    # Modify the fields as per your requirements
    receipt_number: str
    note: str = None
    receipt_type: str
    refund_for: str = None
    order: str = None
    created_at: str
    updated_at: str
    source: str
    receipt_date: str
    cancelled_at: str = None
    total_money: float
    total_tax: float
    points_earned: float
    points_deducted: float
    points_balance: float
    customer_id: str = None
    total_discount: float
    employee_id: str
    store_id: str
    pos_device_id: str
    dining_option: str = None
    total_discounts: List[LineDiscount] = []
    total_taxes: List[LineTax] = []
    tip: float
    surcharge: float
    line_items: List[LineItem] = []


class UpdatedReceipt(BaseModel):
    receipt_number: str
    checkedItems: dict


class Receipt(BaseModel):
    merchant_id: str
    type: str
    created_at: str
    status: str
    receipts: list


class Receipt1(BaseModel):
    receipt_number: str
    note: str
    status: str
    receipt_type: str
    refund_for: str
    order: str
    created_at: str
    updated_at: str
    source: str
    receipt_date: str
    cancelled_at: str
    total_money: float
    total_tax: float
    points_earned: float
    points_deducted: float
    points_balance: float
    customer_id: str
    total_discount: float
    employee_id: str
    store_id: str
    pos_device_id: str
    dining_option: str
    total_discounts: List[LineDiscount] = []
    total_taxes: list
    tip: float
    surcharge: float
    line_items: list
    line_items_total_qty: int


class Receipt2(BaseModel):
    receipt_number: str
    note: str
    status: str
    receipt_type: str
    refund_for: str
    order: str
    created_at: str
    updated_at: str
    source: str
    receipt_date: str
    cancelled_at: str
    total_money: float
    total_tax: float
    points_earned: float
    points_deducted: float
    points_balance: float
    customer_id: str
    total_discount: float
    employee_id: str
    store_id: str
    pos_device_id: str
    dining_option: str
    total_discounts: List[LineDiscount] = []
    total_taxes: list
    tip: float
    surcharge: float
    line_items: list
    cart_items: list
    line_items_total_qty: int
    cart_items_total_qty: int
    missing_items : list
    # grouped_items:list

    # confirmed_items: list


class Confirmed_DataModel(BaseModel):
    receipt_number: str
    note: str
    status: str
    receipt_type: str
    refund_for: str
    order: str
    created_at: str
    updated_at: str
    source: str
    receipt_date: str
    cancelled_at: str
    total_money: float
    total_tax: float
    points_earned: float
    points_deducted: float
    points_balance: float
    customer_id: str
    total_discount: float
    employee_id: str
    store_id: str
    pos_device_id: str
    dining_option: str
    total_discounts: List[LineDiscount] = []
    total_taxes: list
    tip: float
    surcharge: float
    line_items: list
    cart_items: list
    line_items_total_qty: int
    cart_items_total_qty: int
    grouped_items:list
    missing_items:list

    # confirmed_items: list


class Receipt3(BaseModel):
    receipt_number: str
    note: str
    status: str
    receipt_type: str
    refund_for: str
    order: str
    created_at: str
    updated_at: str
    source: str
    receipt_date: str
    cancelled_at: str
    total_money: float
    total_tax: float
    points_earned: float
    points_deducted: float
    points_balance: float
    customer_id: str
    total_discount: float
    employee_id: str
    store_id: str
    pos_device_id: str
    dining_option: str
    total_discounts: List[LineDiscount] = []
    total_taxes: list
    tip: float
    surcharge: float
    line_items: list
    cart_items: list





# receipts = autofirmed_receipts.find().sort("created_at", -1)

#@app.get("/api/get-confirmed-receipts")  # get receipts by their status
# async def get_receipts(status: str = Query(None)):
#     query = {}  # Empty query initially

#     # receipts = list(confirmed_receipts.find())
#     receipts = confirmed_receipts.find().sort("created_at", -1)

#     serialized_receipts = []

#     for receipt in receipts:
#         receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
#         serialized_receipts.append(receipt)

#     return serialized_receipts





@app.delete("/delete_cart_item/{receipt_number}/items/{item_name}")
def delete_item_from_cart(receipt_number: str, item_name: str):
    query = {"receipt_number": receipt_number}
    update = {"$pull": {"cart_items": {"name": item_name}}}
    sales_test_col.update_many(query, update)
    return {"message": f"Deleted {item_name} from the cart."}



@app.put("/edit_cart_item/{receipt_number}/items/{item_name}/quantity")
def update_item_quantity(receipt_number: str, item_name: str, quantity: str):
    print("receiving:", receipt_number, item_name, quantity)
    query = {"receipt_number": receipt_number.strip(), "cart_items.name": item_name.strip()}
    update = {"$set": {"cart_items.$.quantity": quantity}}

    result = sales_test_col.update_one(query, update)

    if result.modified_count == 0:
        # raise HTTPException(status_code=404, detail=f"Item with name {item_name} not found in the cart.")
        return {"message": f"Item with name {item_name} not found in the cart."}


    return {"message": f"Updated quantity of {item_name} to {quantity} in the cart."}









# @app.get("/data-open-in-progress")
# def get_open_in_progress_data():
#     query = {"status": {"$in": ["OPEN", "IN-PROGRESS"]}}
#     receipts = list(sales_test_col.find(query).sort("created_at", -1))

#     serialized_receipts = []

#     for receipt in receipts:
#         receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
#         serialized_receipts.append(receipt)
#     return serialized_receipts


@app.get("/data-open-in-progress")
def get_open_in_progress_data():
    query = {"status": {"$in": ["OPEN", "IN-PROGRESS"]}}
    receipts = list(sales_test_col.find(query).sort("created_at", -1))

    serialized_receipts = []

    for receipt in receipts:
        receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
        receipt_number = receipt["receipt_number"]
        if not confirmed_receipts.find_one({"receipt_number": receipt_number}):
            serialized_receipts.append(receipt)

    return serialized_receipts






@app.delete("/delete-records")
async def delete_records():
    # Delete records with IDs between 1 and 30
    result = sales_test_col.delete_many({"receipt_number": {"$gte": "1", "$lte": "7"}})

    if result.deleted_count > 0:
        return {"message": f"{result.deleted_count} records deleted successfully."}
    else:
        return {"message": "No records found within the specified range."}


@app.put("/update-documents-property")
def update_documents():
    # Update operation
    result = confirmed_receipts.update_many({}, {"$set": {"grouped_items": []}})
    updated_count = result.modified_count

    return {"message": f"Updated {updated_count} documents."}

# **************************************END POINTS BEGIN HERE******************************************************
@app.post("/save_receipt")
async def save_receipt(receipt: Receipt1):
    # Connect to MongoDB
    print(receipt)

    # Save the receipt object to MongoDB
    receipt_data = receipt.dict()
    print("saved:", sales_collection.insert_one(receipt_data))

    # # Close the MongoDB connection
    # client.close()

    return {"message": "Receipt saved successfully"}


@app.post("/save_receipt-refund")
async def save_receipt(receipt: Receipt1):
    # Connect to MongoDB
    print(receipt)

    # Save the receipt object to MongoDB
    receipt_data = receipt.dict()
    print("saved:", refund_collection.insert_one(receipt_data))

    # # Close the MongoDB connection
    # client.close()

    return {"message": "Receipt saved successfully"}


@app.post("/save_receipt-autoconfirmed")
async def save_receipt(receipt: Receipt1):
    # Connect to MongoDB
    print(receipt)

    # Check if the item is already available
    existing_item = autofirmed_receipts.find_one(
        {"receipt_number": receipt.receipt_number})
    if existing_item:
        print("item already exists")
        return {"message": "Item already exists"}

    # Save the receipt object to MongoDB
    receipt_data = receipt.dict()
    print("saved:", autofirmed_receipts.insert_one(receipt_data))

    return {"message": "Auto confirmed Receipt saved successfully"}


@app.post("/save_receipt_cart_items_and_confirmed_items")
async def save_receipt(receipt: Receipt2):
    # Connect to MongoDB
    # print(receipt)
    # print(receipt)
    # Check if the item is already available
    existing_item = sales_test_col.find_one(
        {"receipt_number": receipt.receipt_number})
    if existing_item:
        print("item already exists")
        return {"message": "Item already exists"}

    # Save the receipt object to MongoDB
    receipt_data = receipt.dict()
    print("saved:", sales_test_col.insert_one(receipt_data))

    # # Close the MongoDB connection
    # client.close()

    return {"message": "Receipt saved successfully"}


@app.post("/saves_confirmed_receipts")
async def save_receipt(receipt: Receipt2):
    # Connect to MongoDB
    print(receipt)

    # Save the receipt object to MongoDB
    receipt_data = receipt.dict()
    print("saved:", confirmed_receipts.insert_one(receipt_data))
    # print("saved:", confirmed_receipts.insert_one(receipt_data))

    # # Close the MongoDB connection
    # client.close()

    return {"message": "Receipt saved successfully"}


# @app.post("/receipts")
# async def add_receipt(receipt: Receipt):
#     # Here you can write the code to store the receipt in the database
#     # For demonstration purposes, let's just print the received data
#     receipt_dict = receipt.dict()
#     latest_collection.insert_one(receipt_dict)

#     print(receipt.dict())
#     return {"message": "Receipt added to the database"}


@app.post("/post-receipts/")
async def create_receipt(receipt: Receipt):
    # Convert the receipt to a dictionary
    receipt_dict = receipt.dict()

    print(receipt_dict)

    # Insert the receipt into the MongoDB collection
    refund_collection.insert_one(receipt_dict)

    # Return the saved receipt
    return receipt


@app.post("/test-post-receipts/")
async def create_receipt(receipt: Receipt):
    # Convert the receipt to a dictionary
    receipt_dict = receipt.dict()

    # print(receipt_dict)

    # Insert the receipt into the MongoDB collection
    test_collection.insert_one(receipt_dict)

    # Return the saved receipt
    return receipt


@app.post("/test-post-receipts2/")
async def create_receipt(receipt: Receipt):
    # Convert the receipt to a dictionary
    receipt_dict = receipt.dict()

    try:
        # Insert the receipt into the MongoDB collection
        collection.insert_one(receipt_dict)

    except DuplicateKeyError:
        # Handle duplicate key error if the record already exists
        return {"message": "Duplicate record"}

    # Return the saved receipt
    return receipt


@app.put("/update-receipt/{receipt_number}")
def update_receipt(receipt_number: str, updated_receipt: Receipt2):

    update_data ={
         "status": updated_receipt.status,
        "cart_items_total_qty": updated_receipt.cart_items_total_qty
     }
    # result = sales_test_col.update_one({"receipt_number": receipt_number}, {
    #                                    "$set": {"status": updated_receipt.status}
                                       
                                       
    #                                    })
    result = sales_test_col.update_one({"receipt_number": receipt_number}, {"$set": update_data})

    if result.modified_count == 1:
        return {"message": "Receipt updated successfully"}
    return {"message": "Receipt not found"}


# @app.put("/update-receipt-totalqty/{receipt_number}")
# def update_receipt(receipt_number: str, updated_receipt: Receipt2):
#     result = sales_test_col.update_one({"receipt_number": receipt_number}, {
#                                        "$set": {"status": updated_receipt.status}})
#     if result.modified_count == 1:
#         return {"message": "Receipt updated successfully"}
#     return {"message": "Receipt not found"}


# @app.put("/update-in-progress-status/{receipt_number}")
# async def update_checked_items(receipt_number: str, updated_datas: dict):
#     # print("updated:", updated_datas)
#     # Update the checked items in the MongoDB collection
#     sales_test_col.update_one({"receipt_number": receipt_number}, {
#                           "$set": {"status": "IN-PROGRESS"}})
#     # for x in test_collection.find():
#     #     print(x)
#     return {"message": f"Checked items for receipt {receipt_number} updated successfully"}


# @app.put("/checked-items/{receipt_number}")
# async def update_checked_items(receipt_number: str, updated_datas: dict):
#     # print("updated:", updated_datas)
#     # Update the checked items in the MongoDB collection
#     collection.update_one({"receipt_number": receipt_number}, {
#                           "$set": {"checked_items": updated_datas}})
#     # for x in test_collection.find():
#     #     print(x)
#     return {"message": f"Checked items for receipt {receipt_number} updated successfully"}


# @app.put("/add_cart_items_to_main_receipt/{receipt_number}")
# async def update_checked_items(receipt_number: str, updated_datas: dict):
#     # print("updated:", updated_datas)
#     # Update the checked items in the MongoDB collection
#     sales_test_col.update_one({"receipt_number": receipt_number}, {
#         "$push": {"cart_items": updated_datas}})
#     # for x in test_collection.find():
#     #     print(x)
#     return {"message": f"Checked items for receipt {receipt_number} updated successfully"}


@app.put("/add_cart_items_to_main_receipt/{receipt_number}")
async def update_checked_items(receipt_number: str, updated_datas: dict):
    # print("updated datas", updated_datas)
    # Check if the object already exists in the cart_items array
    existing_item = sales_test_col.find_one(
        {"receipt_number": receipt_number, "cart_items": updated_datas})
    if existing_item:
        return {"message": f"Object already exists in cart_items for receipt {receipt_number}"}

    # Update the checked items in the MongoDB collection
    sales_test_col.update_one({"receipt_number": receipt_number}, {
        "$push": {"cart_items": updated_datas}})

    return {"message": f"Checked items for receipt {receipt_number} updated successfully"}


@app.put("/add_missing_items_to_main_receipt/{receipt_number}")
def update_document(receipt_number: str, missing_items: list):
    query = {"receipt_number": receipt_number}
    update = {"$set": {"missing_items": missing_items}}
    result = sales_test_col.update_one(query, update)

    if result.matched_count == 0:
        return {"message": "Document not found"}

    return {"message": "Document updated successfully"}




@app.put("/add_missing_items_to_confirmed_receipt/{receipt_number}")
def update_document(receipt_number: str, missing_items: list):
    query = {"receipt_number": receipt_number}
    update = {"$set": {"missing_items": missing_items}}
    result = confirmed_receipts.update_one(query, update)

    if result.matched_count == 0:
        return {"message": "Document not found"}

    return {"message": "Document updated successfully"}



@app.put("/add_grouped_items_to_main_receipt/{receipt_number}")
def update_document(receipt_number: str, missing_items: list):
    query = {"receipt_number": receipt_number}
    update = {"$set": {"grouped_items": missing_items}}
    result = sales_test_col.update_one(query, update)

    if result.matched_count == 0:
        return {"message": "Document not found"}

    return {"message": "New Grouped items updated to the document"}


@app.put("/add_grouped_items_to_refund/{receipt_number}")
def update_document(receipt_number: str, missing_items: list):
    query = {"receipt_number": receipt_number}
    update = {"$set": {"grouped_items": missing_items}}
    result = refund_collection.update_one(query, update)

    if result.matched_count == 0:
        return {"message": "Document not found"}

    return {"message": "New Grouped items updated to the document"}


@app.put("/add_grouped_items_to_auto-confirmed_receipt/{receipt_number}")
def update_document(receipt_number: str, missing_items: list):
    query = {"receipt_number": receipt_number}
    update = {"$set": {"grouped_items": missing_items}}
    result = autofirmed_receipts.update_one(query, update)

    if result.matched_count == 0:
        return {"message": "Document not found in autoconfirmed collection"}

    return {"message": "New Grouped items updated to the autoconfirmed collection"}


@app.put("/duplicate-checker-for-search/{receipt_number}")
async def update_checked_items(receipt_number: str, updated_datas: dict):
    # Check if the object already exists in the cart_items array
    existing_item = sales_test_col.find_one(
        {"receipt_number": receipt_number, "cart_items": updated_datas})
    if existing_item:
        # return {"message": f"Object already exists in cart_items for receipt {receipt_number}"}
        return {"message": f"duplicate"}

    else:
        return {"message": f"unique"}


@app.get("/get-cart_items_in_receipt/{receipt_number}")
def get_checked_items(receipt_number: str):
    # Find the document with the specified receipt_number
    document = sales_test_col.find_one({"receipt_number": str(receipt_number)})

    if document:
        document["_id"] = str(document["_id"])  # Convert ObjectId to string
        return document
    else:
        return {"message": "Document not found"}


@app.get("/get-cart_items_in_auto_confirmed/{receipt_number}")
def get_checked_items(receipt_number: str):
    # Find the document with the specified receipt_number
    document = autofirmed_receipts.find_one(
        {"receipt_number": str(receipt_number)})

    if document:
        document["_id"] = str(document["_id"])  # Convert ObjectId to string
        return document
    else:
        return {"message": "Document not found"}


@app.get("/get-cart_items_in_refund/{receipt_number}")
def get_checked_items(receipt_number: str):
    # Find the document with the specified receipt_number
    document = refund_collection.find_one(
        {"receipt_number": str(receipt_number)})

    if document:
        document["_id"] = str(document["_id"])  # Convert ObjectId to string
        # //return document
        return JSONResponse(content=document)
    else:
        # return {"message": "Document not found"}
        return JSONResponse(content={"message": "Document not found"})


@app.get("/get-cart_items_in_confirmed/{receipt_number}")
def get_checked_items(receipt_number: str):
    # Find the document with the specified receipt_number
    document = confirmed_receipts.find_one(
        {"receipt_number": str(receipt_number)})

    if document:
        document["_id"] = str(document["_id"])  # Convert ObjectId to string
        return document
    else:
        return {"message": "Document not found"}


@app.get("/get-receipt-stats/{receipt_number}")
def get_checked_items(receipt_number: str):
    # Find the document with the specified receipt_number
    print("r_number", receipt_number)
    document = sales_test_col.find_one({"receipt_number": str(receipt_number)})

    if document:
        document["_id"] = str(document["_id"])  # Convert ObjectId to string
        return document
    else:
        return {"message": "Document not found"}


# ***************************************************************************************************************/


@app.put("/update_checked_item_name/{receipt_number}/{old_item_name}/{new_item_name}/{old_quantity}/{new_quantity}/{old_item_status}/{new_item_status}/{old_quantity_status}/{new_quantity_status}")
async def update_checked_item_name(receipt_number: str,
                                   old_item_name: str, new_item_name: str,
                                   old_quantity: str, new_quantity: str,
                                   old_item_status: str, new_item_status: str,
                                   old_quantity_status: str, new_quantity_status: str):

    receipt_number = receipt_number.strip()
    old_item_name = old_item_name.strip()
    new_item_name = new_item_name.strip()
    old_quantity = int(old_quantity.strip())
    new_quantity = int(new_quantity.strip())
    old_item_status = old_item_status.strip()
    new_item_status = new_item_status.strip()
    old_quantity_status = old_quantity_status.strip()
    new_quantity_status = new_quantity_status.strip()

    print("receipt number:", receipt_number)
    print("old_item_name:", old_item_name)
    print("new item name:", new_item_name)
    print("old quantity:", "incorrect")
    print("new quantity:", new_quantity)
    print("old_item_stattus:", old_item_status)
    print("new_item_status:", new_item_status)

    print("old_quantity_status:", old_quantity_status)
    print("new_quantity_status:", new_quantity_status)
    # Find the document by its ID
    document = collection.find_one({"receipt_number": receipt_number.strip()})

    # print("doc", document)

    if document:
        print("document")
        # Update the "checkedItems" array
        for item in document["checked_items"]["checkedItems"]:
            print("provided old item name provided:", old_item_name,
                  " | correct old item name:", item["itemName"])
            if item["itemName"] == old_item_name.strip():
                print("found item, changing in progres")
                item["itemName"] = new_item_name.strip()
                print("modified itemName:", item['itemName'])
            else:
                print(" item name not found")

            print(" ==> provided old item quantity", type(old_quantity),
                  " |  correct old item quantity:", item["quantity"])
            if item["quantity"] == old_quantity:
                print("FOUND ITEM QUANTITY")
                item["quantity"] = new_quantity
                print("MODIFIED itemQuantity:", item['quantity'])

            else:
                print("==>item quantity not found")

            if item["itemNameStatus"] == old_item_status.strip():
                print("FOUND ITEM NAME STATUS")
                item["itemNameStatus"] = new_item_status.strip()

            if item["quantityStatus"] == old_quantity_status.strip():
                print("FOUND ITEM QUANTITY STATUS")
                item["quantityStatus"] = new_quantity_status.strip()

            else:
                print("item quantity not found")

        # Save the updated document back to the collection
        collection.replace_one(
            {"receipt_number": receipt_number}, document)

        return {"message": "Checked item name updated successfully."}

    return {"message": "Document not found."}


@app.get("/checked-items/{receipt_number}")
def get_checked_items(receipt_number: str):
    # Find the document with the specified receipt_number
    document = collection.find_one({"receipt_number": str(receipt_number)})

    if document:
        document["_id"] = str(document["_id"])  # Convert ObjectId to string
        return document
    else:
        return {"message": "Document not found"}


@app.get("/search-receipts/{receipt_id}")
async def get_receipt(receipt_id: str):
    receipt = collection.find_one({"_id": receipt_id})
    if receipt:
        return list(receipt)
    else:
        return {"message": "Receipt not found"}


@app.get("/api/search")
def search(receipt_number: str):
    results = collection.find({"receipt_number": receipt_number})
    return list(results)


# @app.get("/api/get-receipts")
# async def get_all_receipts():
#     receipts = list(latest_collection.find())
#     # print(receipts)
#     serialized_receipts = []

#     for receipt in receipts:
#         receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
#         serialized_receipts.append(receipt)

#     # print(serialized_receipts)
#     return serialized_receipts


# @app.get("/api/get-open-receipts")
# async def get_all_receipts():
#     receipts = list(sales_collection.find())
#     # print(receipts)
#     serialized_receipts = []

#     for receipt in receipts:
#         receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
#         serialized_receipts.append(receipt)

#     # print(serialized_receipts)
#     return serialized_receipts


# @app.get("/api/get-receipts-type")  # get receipts by their status
# async def get_receipts(status: str = Query(None)):
#     query = {}  # Empty query initially

#     if status:
#         query["status"] = status

#     receipts = list(sales_test_col.find(query))
#     serialized_receipts = []

#     for receipt in receipts:
#         receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
#         serialized_receipts.append(receipt)

#     return serialized_receipts


@app.get("/api/get-receipts-type")
async def get_receipts(status: str = Query(None)):
    query = {}  # Empty query initially

    if status:
        query["status"] = status

    receipts = list(sales_test_col.find(query))
    serialized_receipts = []

    for receipt in receipts:
        receipt["receipt_number"] = str(
            receipt["receipt_number"])  # Convert ObjectId to string

        # Check if the receipt is available in the confirmed_receipts collection
        if not confirmed_receipts.find_one({"receipt_number": receipt["receipt_number"]}):
            receipt["confirmed"] = False
            receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
#         serialized_receipts.append(receipt)
            serialized_receipts.append(receipt)

    return serialized_receipts


@app.get("/api/get-open-receipts-type")
async def get_receipts(status: List[str] = Query(None)):
    query = {}

    if status:
        query["status"] = {"$in": status}

    receipts = list(sales_test_col.find(query))
    serialized_receipts = []

    for receipt in receipts:
        receipt["receipt_number"] = str(receipt["receipt_number"])

        if not confirmed_receipts.find_one({"receipt_number": receipt["receipt_number"]}):
            receipt["confirmed"] = False
            receipt["_id"] = str(receipt["_id"])
            serialized_receipts.append(receipt)
    return serialized_receipts


@app.get("/api/get-confirmed-receipts")  # get receipts by their status
async def get_receipts(status: str = Query(None)):
    query = {}  # Empty query initially

    # receipts = list(confirmed_receipts.find())
    receipts = confirmed_receipts.find().sort("created_at", -1)

    serialized_receipts = []

    for receipt in receipts:
        receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
        serialized_receipts.append(receipt)

    return serialized_receipts


@app.get("/api/get-auto-confirmed-receipts")  # get receipts by their status
async def get_receipts(status: str = Query(None)):
    query = {}  # Empty query initially

    # receipts = list(confirmed_receipts.find())
    receipts = autofirmed_receipts.find().sort("created_at", -1)

    serialized_receipts = []

    for receipt in receipts:
        receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
        serialized_receipts.append(receipt)

    return serialized_receipts


@app.get("/api/get-confirmed-receipt_by_receipt_number/{receipt_number}")
async def get_receipts(receipt_number: str):
    query = {"receipt_number": receipt_number}

    receipts = list(confirmed_receipts.find(query))
    serialized_receipts = []

    for receipt in receipts:
        receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
        serialized_receipts.append(receipt)

    return serialized_receipts


# @app.get("/api/get-receipts-refund")  # get receipts by their status
# async def get_receipts(status: str = Query(None)):
#     query = {}  # Empty query initially

#     if status:
#         query["status"] = status

#     # receipts = list(refund_collection.find(query))
#     receipts = list(refund_collection.find(query).sort("created_at", ASCENDING))

#     serialized_receipts = []

#     for receipt in receipts:
#         receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
#         serialized_receipts.append(receipt)

#     return serialized_receipts[::1]


# @app.get("/api/get-receipts-refund")  # get receipts by their status
# async def get_receipts(status: str = Query(None)):
#     query = {}

#     if status:
#         query["status"] = status

#     receipts = list(refund_collection.find(
#         query).sort("created_at", ASCENDING))
#     serialized_receipts = []

#     for receipt in receipts:
#         receipt["_id"] = str(receipt["_id"])
#         serialized_receipts.insert(0, receipt)  # Insert at the beginning

#     return serialized_receipts

@app.get("/api/get-receipts-refund")  # get receipts by their status
async def get_receipts(status: str = Query(None)):
    query = {}

    if status:
        query["status"] = status

    receipts = refund_collection.find(query).sort("created_at", ASCENDING)
    serialized_receipts = []

    seen_ids = set()

    for receipt in receipts:
        receipt_id = str(receipt["_id"])

        if receipt_id not in seen_ids:
            receipt["_id"] = str(receipt["_id"])
            serialized_receipts.insert(0, receipt)
            seen_ids.add(receipt_id)

    return serialized_receipts


@app.get("/api/get-open-receipts")
async def get_all_receipts():
    receipts = list(latest_collection.find({"status": "OPEN"}))
    serialized_receipts = []

    for receipt in receipts:
        receipt["_id"] = str(receipt["_id"])  # Convert ObjectId to string
        serialized_receipts.append(receipt)
    return serialized_receipts


@app.delete("/delete_document/{receipt_number}")
async def delete_document(receipt_number: str):
    # Find the document by its ID
    document = collection.find_one({"receipt_number": receipt_number})

    if document:
        # Delete the document
        collection.delete_one({"receipt_number": receipt_number})
        return {"message": "Document deleted successfully."}
    else:
        return {"message": "Document not found."}


@app.delete("/delete_all")
def delete_all_data():
    # Delete all documents in the collection
    result = test_collection.delete_many({})
    return {"message": f"Deleted {result.deleted_count} documents"}
