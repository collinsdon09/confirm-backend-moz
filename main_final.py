from typing import List
from fastapi import FastAPI
from pydantic import BaseModel
from pymongo import MongoClient, ASCENDING
from pymongo.results import DeleteResult
from fastapi.middleware.cors import CORSMiddleware
from bson import ObjectId
import websocket
import asyncio
from pymongo.errors import DuplicateKeyError
from fastapi import Query



app = FastAPI()
origins = [

    "http://localhost:5173",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



client = MongoClient("mongodb://localhost:27017")
db = client["receipts_db"]
collection = db["incoming_receipts"]
test_collection = db["test_collection"]
latest_collection = db["new_receipts2"]
refund_collection = db["refunds"]
sales_collection = db["sales"]
sales_test_col = db["sales_test"]
confirmed_receipts = db['confirmed_receipts']





class Receipt3(BaseModel):
    receipt_number: str
    note: str
    status: str
    receipt_type: str
    refund_for: str
    order: str
    created_at: str
    updated_at: str
    source: str
    receipt_date: str
    cancelled_at: str
    total_money: float
    total_tax: float
    points_earned: float
    points_deducted: float
    points_balance: float
    customer_id: str
    total_discount: float
    employee_id: str
    store_id: str
    pos_device_id: str
    dining_option: str
    total_discounts: list
    total_taxes: list
    tip: float
    surcharge: float
    line_items: list
    cart_items: list





@app.post("/save-receipt-final")
async def save_receipt(receipt: Receipt3):
    # Connect to MongoDB
    print(receipt)

    # Save the receipt object to MongoDB
    receipt_data = receipt.dict()
    print("saved:", sales_test_col.insert_one(receipt_data))

    # # Close the MongoDB connection
    # client.close()

    return {"message": "Receipt saved successfully"}



@app.put("/update-documents")
def update_documents():
    # Update operation
    result = sales_collection.update_many({}, {"$set": {"missing_items": []}})
    updated_count = result.modified_count
    
    return {"message": f"Updated {updated_count} documents."}