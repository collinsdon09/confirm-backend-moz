const WebSocket = require("ws");

// Create a WebSocket server
const ws = new WebSocket.Server({ port: 8080 });
let messageCount = 0;

// const incoming_test = {
//   _id: { $oid: "6490a4617c767afa448f43ca" },
//   merchant_id: "62190099-0b95-40b1-a094-2a8bf515172f",
//   type: "receipts.update",
//   created_at: "2023-06-25T12:40:42.524Z",
//   status: "OPEN",
//   receipts: [
//     {
//       receipt_number: "grouped_items13",
//       note: null,
//       receipt_type: "SALE",
//       refund_for: "10-46563",
//       order: "Egide Somaliano - 2:26 PM Diversos",
//       created_at: "2023-06-26T10:15:36.000Z",
//       updated_at: "2023-06-25T17:53:36.000Z",
//       source: "point of sale",
//       receipt_date: "2023-06-19T17:53:33.000Z",
//       cancelled_at: null,
//       total_money: 10,
//       total_tax: 73.1,
//       points_earned: 0,
//       points_deducted: 1.32,
//       points_balance: 710.42,
//       customer_id: "f4f63dbe-1b6a-47ae-9953-46e5b771d11f",
//       total_discount: 0,
//       employee_id: "2a2fe1c7-01ae-44f6-a654-fba26205df2e",
//       store_id: "abc0c392-258d-45fd-b407-4f9ef77930bd",
//       pos_device_id: "e17c5e88-d673-4f18-8d61-90f3988273fd",
//       dining_option: null,

//       total_discounts: [],
//       total_taxes: [
//         {
//           id: "29538479-a88d-42ff-bc73-60641fc16296",
//           type: "INCLUDED",
//           name: "IVA",
//           rate: 16,
//           money_amount: 73.1,
//         },
//       ],
//       tip: 0,
//       surcharge: 0,
//       line_items: [
//         {
//           id: "9553152a-9d95-ded2-f6a3-2abcdc6b516c",
//           item_id: "bf0d30be-e09e-42d8-8a46-50cb408ff117",
//           variant_id: "ffe69b99-c756-4216-a34f-e9488192c2c6",
//           item_name: "Arroz Adam 25kg",
//           variant_name: null,
//           sku: "10211",
//           quantity: 1,
//           price: 1200,
//           gross_total_money: 1200,
//           total_money: 1200,
//           cost: 0,
//           cost_total: 0,
//           line_note: null,
//           line_taxes: [],
//           total_discount: 0,
//           line_discounts: [],
//           line_modifiers: [],
//         },
//         {
//           id: "9553152a-9d95-ded2-f6a3-2abcdc6b516d",
//           item_id: "05fc7fcf-a15f-4482-8b5e-93410ab83b67",
//           variant_id: "5680978f-9e9f-47f0-aca6-b1a2833f8f6d",
//           item_name: "Arromate Top Class Spices 350g (caixa=12)",
//           variant_name: null,
//           sku: "11284",
//           quantity: 1,
//           price: 1000,
//           gross_total_money: 1000,
//           total_money: 1000,
//           cost: 950,
//           cost_total: 950,
//           line_note: null,
//           line_taxes: [
//             {
//               money_amount: 137.93,
//               id: "29538479-a88d-42ff-bc73-60641fc16296",
//               type: "INCLUDED",
//               name: "IVA",
//               rate: 16,
//             },
//           ],
//           total_discount: 0,
//           line_discounts: [],
//           line_modifiers: [],
//         },
//         {
//           id: "9553152a-9d95-ded2-f6a3-2abcdc6b516e",
//           item_id: "bf0d30be-e09e-42d8-8a46-50cb408ff117",
//           variant_id: "ffe69b99-c756-4216-a34f-e9488192c2c6",
//           item_name: "Arroz Adam 25kg",
//           variant_name: null,
//           sku: "10211",
//           quantity: 2,
//           price: 1200,
//           gross_total_money: 2400,
//           total_money: 2400,
//           cost: 0,
//           cost_total: 0,
//           line_note: null,
//           line_taxes: [],
//           total_discount: 0,
//           line_discounts: [],
//           line_modifiers: [],
//         },
//         {
//           id: "9553152a-9d95-ded2-f6a3-2abcdc6b516f",
//           item_id: "05fc7fcf-a15f-4482-8b5e-93410ab83b67",
//           variant_id: "5680978f-9e9f-47f0-aca6-b1a2833f8f6d",
//           item_name: "Arromate Top Class Spices 350g (caixa=12)",
//           variant_name: null,
//           sku: "11284",
//           quantity: 2,
//           price: 1000,
//           gross_total_money: 2000,
//           total_money: 2000,
//           cost: 950,
//           cost_total: 1900,
//           line_note: null,
//           line_taxes: [
//             {
//               money_amount: 275.86,
//               id: "29538479-a88d-42ff-bc73-60641fc16296",
//               type: "INCLUDED",
//               name: "IVA",
//               rate: 16,
//             },
//           ],
//           total_discount: 0,
//           line_discounts: [],
//           line_modifiers: [],
//         },
//       ],

//       line_items_total_qty: 0,
//       cart_items_total_qty: 0,
//     },
//   ],
// };

// Function to send messages to connected clients
function sendMessages() {
    const currentTime = new Date().toISOString();
  messageCount++;

  const incoming_test = {
    _id: { $oid: "6490a4617c767afa448f43ca" },
    merchant_id: "62190099-0b95-40b1-a094-2a8bf515172f",
    type: "receipts.update",
    created_at: currentTime,
    status: "OPEN",
    receipts: [
      {
        receipt_number: "REFUND TEST",
        note: null,
        receipt_type: "SALE",
        refund_for: "10-46563",
        order: "Egide Somaliano - 2:26 PM Diversos",
        created_at: currentTime,
        updated_at: "2023-06-25T17:53:36.000Z",
        source: "point of sale",
        receipt_date: "2023-06-19T17:53:33.000Z",
        cancelled_at: null,
        total_money: 10,
        total_tax: 73.1,
        points_earned: 0,
        points_deducted: 1.32,
        points_balance: 710.42,
        customer_id: "f4f63dbe-1b6a-47ae-9953-46e5b771d11f",
        total_discount: 0,
        employee_id: "2a2fe1c7-01ae-44f6-a654-fba26205df2e",
        store_id: "abc0c392-258d-45fd-b407-4f9ef77930bd",
        pos_device_id: "e17c5e88-d673-4f18-8d61-90f3988273fd",
        dining_option: null,
  
        total_discounts: [],
        total_taxes: [
          {
            id: "29538479-a88d-42ff-bc73-60641fc16296",
            type: "INCLUDED",
            name: "IVA",
            rate: 16,
            money_amount: 73.1,
          },
        ],
        tip: 0,
        surcharge: 0,
        line_items: [
          {
            id: "9553152a-9d95-ded2-f6a3-2abcdc6b516c",
            item_id: "bf0d30be-e09e-42d8-8a46-50cb408ff117",
            variant_id: "ffe69b99-c756-4216-a34f-e9488192c2c6",
            item_name: "Arroz Adam 25kg",
            variant_name: null,
            sku: "10211",
            quantity: 1,
            price: 1200,
            gross_total_money: 1200,
            total_money: 1200,
            cost: 0,
            cost_total: 0,
            line_note: null,
            line_taxes: [],
            total_discount: 0,
            line_discounts: [],
            line_modifiers: [],
          },
          {
            id: "9553152a-9d95-ded2-f6a3-2abcdc6b516d",
            item_id: "05fc7fcf-a15f-4482-8b5e-93410ab83b67",
            variant_id: "5680978f-9e9f-47f0-aca6-b1a2833f8f6d",
            item_name: "Arromate Top Class Spices 350g (caixa=12)",
            variant_name: null,
            sku: "11284",
            quantity: 1,
            price: 1000,
            gross_total_money: 1000,
            total_money: 1000,
            cost: 950,
            cost_total: 950,
            line_note: null,
            line_taxes: [
              {
                money_amount: 137.93,
                id: "29538479-a88d-42ff-bc73-60641fc16296",
                type: "INCLUDED",
                name: "IVA",
                rate: 16,
              },
            ],
            total_discount: 0,
            line_discounts: [],
            line_modifiers: [],
          },
          {
            id: "9553152a-9d95-ded2-f6a3-2abcdc6b516e",
            item_id: "bf0d30be-e09e-42d8-8a46-50cb408ff117",
            variant_id: "ffe69b99-c756-4216-a34f-e9488192c2c6",
            item_name: "Arroz Adam 25kg",
            variant_name: null,
            sku: "10211",
            quantity: 2,
            price: 1200,
            gross_total_money: 2400,
            total_money: 2400,
            cost: 0,
            cost_total: 0,
            line_note: null,
            line_taxes: [],
            total_discount: 0,
            line_discounts: [],
            line_modifiers: [],
          },
          {
            id: "9553152a-9d95-ded2-f6a3-2abcdc6b516f",
            item_id: "05fc7fcf-a15f-4482-8b5e-93410ab83b67",
            variant_id: "5680978f-9e9f-47f0-aca6-b1a2833f8f6d",
            item_name: "Arromate Top Class Spices 350g (caixa=12)",
            variant_name: null,
            sku: "11284",
            quantity: 2,
            price: 1000,
            gross_total_money: 2000,
            total_money: 2000,
            cost: 950,
            cost_total: 1900,
            line_note: null,
            line_taxes: [
              {
                money_amount: 275.86,
                id: "29538479-a88d-42ff-bc73-60641fc16296",
                type: "INCLUDED",
                name: "IVA",
                rate: 16,
              },
            ],
            total_discount: 0,
            line_discounts: [],
            line_modifiers: [],
          },
        ],
  
        line_items_total_qty: 0,
        cart_items_total_qty: 0,
      },
    ],
  };
  

  

  const modifiedObject = { ...incoming_test };
  modifiedObject.receipts.forEach(receipt => {
    receipt.receipt_number = messageCount;
  });

  const message = JSON.stringify(modifiedObject);


  ws.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(message);
    }
  });
}

// Schedule the message sending every 30 seconds
setInterval(sendMessages, 3000);

// Event handler for new client connections
ws.on("connection", (ws) => {
  console.log("New client connected");

  // Event handler for client messages
  ws.on("message", (message) => {
    console.log(`Received message: ${message}`);
  });

  // Event handler for client disconnections
  ws.on("close", () => {
    console.log("Client disconnected");
  });

  // Send an initial message to the newly connected client
  
});
